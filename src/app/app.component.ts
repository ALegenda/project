import {Component} from '@angular/core';
import {Router} from "@angular/router";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  navbar = false;
  constructor(public router: Router) {
    router.events.subscribe((url: any) => {
      this.navbar = router.url !== '/'
    });

  }
}
