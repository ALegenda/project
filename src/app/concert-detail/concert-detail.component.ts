import {Component, OnInit} from '@angular/core';
import {Concert} from "../shared/concert";
import {ActivatedRoute, Router} from "@angular/router";
import {UserService} from "../services/user.service";
import 'rxjs/Rx';

@Component({
  selector: 'app-concert-detail',
  templateUrl: './concert-detail.component.html',
  styleUrls: ['./concert-detail.component.scss']
})
export class ConcertDetailComponent implements OnInit {

  concert: Concert;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private userservice: UserService) {
  }

  ngOnInit() {
    if (this.concert) {
      return;
    }


    this.route.params
      .switchMap(({id}) => this.userservice.getConcert(id))
      .subscribe((concert) => {
        if (concert) {
          this.concert = concert;
          return;
        }
      });


  }

  onBuyClick() {
    this.userservice.buy(this.concert._id).subscribe((err)=>{
      if (err === 'error') {
        return;
      }
      console.log(err);
      this.router.navigate(['/concerts']);
    });

  }
}
