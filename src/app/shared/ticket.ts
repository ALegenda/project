export class Ticket {
  _id: string;
  concert: string;
  user: string;
  isUsed: boolean;
}
