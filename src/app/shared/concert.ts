export class Concert{
  _id : string;
  name : string;
  image : string;
  date : string;
  description : string;
  price : string;
  creator : string;
}
