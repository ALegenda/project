import {Component, OnInit} from '@angular/core';
import {Concert} from "../../shared/concert";
import {UserService} from "../../services/user.service";

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss']
})
export class EventComponent implements OnInit {

  concerts: Concert[];


  constructor(private userservice: UserService) {
  }

  ngOnInit() {
    this.getConcerts();
  }

  private getConcerts() {
    this.userservice.getMyConcerts().subscribe(data => this.concerts = data);
  }

  private onClick(concert : Concert){
    console.log(concert);
  }
}
