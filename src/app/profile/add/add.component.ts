import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../../services/user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {
  regForm: FormGroup;

  constructor(private fb: FormBuilder, private userservice: UserService, private router: Router) {
    this.regForm = fb.group({
      defaultFormDescription: ['', [Validators.required]],
      defaultFormDate: ['', [Validators.required]],
      defaultFormName: ['', [Validators.required]],
      defaultFormPrice: ['', [Validators.required]],
      defaultFormImage: ['', [Validators.required]]
    });
  }

  ngOnInit() {
  }

  Add() {
    if (this.regForm.invalid) {
      return;
    }

    const Description = this.regForm.value.defaultFormDescription;
    const Date = this.regForm.value.defaultFormDate;
    const Price = this.regForm.value.defaultFormPrice;
    const Name = this.regForm.value.defaultFormName;
    const Image = this.regForm.value.defaultFormImage;

    this.userservice.addEvent(Name, Image, Date, Description, Price).subscribe((err) => {
      if (err === 'error') {
        this.regForm.reset();
        return;
      }
      console.log(localStorage.getItem('token'));
      this.router.navigate(['/concerts']);
    });
  }
}
