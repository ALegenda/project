import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  infoActive: boolean;
  eventActive: boolean;
  addActive: boolean;
  boughtActive: boolean;

  constructor(private router : Router) {
    this.infoActive=true;
  }

  ngOnInit() {
    if(!localStorage.getItem('token')){
      this.router.navigate(['/login']);
    }
  }

  infoClick() {
    this.infoActive=true;
    this.eventActive=false;
    this.addActive=false;
    this.boughtActive=false;
  }

  eventsClick() {
    this.infoActive=false;
    this.eventActive=true;
    this.addActive=false;
    this.boughtActive=false;
  }

  addClick() {
    this.infoActive=false;
    this.eventActive=false;
    this.addActive=true;
    this.boughtActive=false;
  }

  boughtClick() {
    this.infoActive=false;
    this.eventActive=false;
    this.addActive=false;
    this.boughtActive=true;
  }
}
