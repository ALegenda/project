import {Component, OnInit} from '@angular/core';
import {UserService} from "../../services/user.service";
import {Ticket} from "../../shared/ticket";

@Component({
  selector: 'app-bought',
  templateUrl: './bought.component.html',
  styleUrls: ['./bought.component.scss']
})
export class BoughtComponent implements OnInit {


  tickets: Ticket[];

  constructor(private userservice: UserService) {
  }

  ngOnInit() {
    this.getTickets();
  }

  private getTickets() {
    this.userservice.getMyTickets().subscribe(data => {

      this.tickets = data;
    });
  }

  getqr(ticket: Ticket) {
    console.log(ticket);
    console.log(JSON.stringify(ticket));
    return JSON.stringify(ticket);
  }
}
