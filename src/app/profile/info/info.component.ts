import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../../services/user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss']
})
export class InfoComponent implements OnInit {

  regForm: FormGroup;

  constructor(private fb: FormBuilder, private userservice: UserService, private router: Router) {
    this.regForm = fb.group({
      defaultFormLogin: ['', [Validators.required]],
      defaultFormName: ['', [Validators.required]],
      defaultFormSurname: ['', [Validators.required]],
      defaultFormEmail: ['', [Validators.required]]
    });
  }

  ngOnInit() {
    this.userservice.getMyInfo().subscribe((data) => {
      this.regForm.setValue({
        defaultFormLogin: data.login,
        defaultFormEmail:data.email,
        defaultFormName:data.name,
        defaultFormSurname:data.surname
      });
    })
  }

}
