import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {Concert} from "../shared/concert";
import {User} from "../shared/user";
import 'rxjs/add/operator/map';
import {Ticket} from "../shared/ticket";


@Injectable()
export class UserService {

  constructor(private http: HttpClient) {
  }

  getConcerts(): Observable<Concert[]> {
    return this.http.get<Concert[]>(`http://localhost:5000/api/concerts`, {responseType: "json"});
  }

  getMyConcerts(): Observable<Concert[]> {
    return this.http.get<Concert[]>(`http://localhost:5000/api/concerts/my`, this.jwt());
  }

  getConcert(id): Observable<Concert> {
    return this.http.get<Concert>(`http://localhost:5000/api/concert/${id}`, {responseType: "json"});
  }

  login(login: string, password: string) {
    return this.http.post<{ message: string, token: string }>('http://localhost:5000/api/login', JSON.stringify({
        login,
        password
      }),
      {headers: new HttpHeaders().set('Content-Type', 'application/json')})
      .do((response) => {
        // login successful if there's a jwt token in the response
        // const user = response.json();
        // console.log(user);
        console.log(response);
        if (response) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('token', response.token);
        }
      }).catch(() => Observable.of('error'));
  }

  addEvent(name: string, image: string, date: string, description: string, price: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      })
    };
    return this.http.post<{ message: string }>('http://localhost:5000/api/concert', JSON.stringify({
      name,
      image,
      date,
      description,
      price
    }), httpOptions)
      .do((response) => {
        // login successful if there's a jwt token in the response
        // const user = response.json();
        // console.log(user);
        console.log(response);
      }).catch(() => Observable.of('error'));
  }

  register(email: string, password: string, login: string, name: string, surname: string) {
    return this.http.post<{ message: string, token: string }>('http://localhost:5000/api/register', JSON.stringify({
        login,
        password,
        name,
        surname,
        email
      }),
      {headers: new HttpHeaders().set('Content-Type', 'application/json')})
      .do((response) => {
        // login successful if there's a jwt token in the response
        // const user = response.json();
        // console.log(user);
        console.log(response);
        if (response) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('token', response.token);
        }
      }).catch(() => Observable.of('error'));
  }

  getMyInfo() {
    return this.http.get<User>(`http://localhost:5000/api/user`, this.jwt());
  }

  buy(_id: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      })
    };
    return this.http.post<{ message: string }>(`http://localhost:5000/api/buy`,
      JSON.stringify({concert: _id}),
      httpOptions
    ).do((response) => {
      // login successful if there's a jwt token in the response
      // const user = response.json();
      // console.log(user);
      console.log(response);
    }).catch(() => Observable.of('error'));
  }

  private jwt() {
    // create authorization header with jwt token
    const currentUser = localStorage.getItem('token');
    if (currentUser) {
      const headers = new HttpHeaders()
        .set("Authorization", `Bearer ${currentUser}`);
      headers.set('Content-Type', 'application/json');
      return {headers};
    }
  }


  getMyTickets() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      })
    };
    return this.http.get<Ticket[]>(
      `http://localhost:5000/api/tickets/my`,
      httpOptions);
  }
}
