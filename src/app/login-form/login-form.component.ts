import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from "../services/user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {
  ngOnInit() {
    if (localStorage.getItem('token'))
      this.router.navigate(['/concerts'])
  }

  loginForm: FormGroup;

  constructor(private fb: FormBuilder, private userservice: UserService, private router: Router) {
    this.loginForm = fb.group({
      defaultFormEmail: ['', [Validators.required]],
      defaultFormPass: ['', [Validators.required, Validators.minLength(4)]]
    });
  }

  LogIn() {
    if (this.loginForm.invalid) {
      return;
    }

    const email = this.loginForm.value.defaultFormEmail;
    const pass = this.loginForm.value.defaultFormPass;
    console.log('kek');
    this.userservice.login(email, pass).subscribe((err) => {
      if (err === 'error') {
        this.loginForm.reset();
        return;
      }
      console.log(localStorage.getItem('token'));
      this.router.navigate(['/concerts']);
    });

  }
}
