import {Component, OnInit, Output} from '@angular/core';

import {Concert} from "../shared/concert";
import {ActivatedRoute, Router} from "@angular/router";
import {UserService} from "../services/user.service";



@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {


  concerts : Concert[];


  onClick(concert: Concert){
    this.router.navigate([`concerts/${concert._id}`]);
    console.log(this.router);
  }

  constructor(private route: ActivatedRoute, private router: Router, private userservice: UserService) {
  }

  ngOnInit() {
    this.getConcerts();
  }

  getConcerts(){
    this.userservice.getConcerts().subscribe(data => this.concerts = data);
  }

}
