import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {MatButtonModule, MatCardModule, MatGridListModule, MatListModule, MatToolbarModule} from '@angular/material';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

import {FlexLayoutModule} from "@angular/flex-layout";


import {AppComponent} from './app.component';
import {MenuComponent} from './menu/menu.component';
import {ConcertDetailComponent} from './concert-detail/concert-detail.component';
import {RouterModule, Routes} from "@angular/router";
import {appRoutes} from "./app.routing";
import { LandingComponent } from './landing/landing.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { SideBarComponent } from './side-bar/side-bar.component';
import {UserService} from "./services/user.service";
import { RegisterComponent } from './register/register.component';
import { ProfileComponent } from './profile/profile.component';
import { InfoComponent } from './profile/info/info.component';
import { BoughtComponent } from './profile/bought/bought.component';
import { EventComponent } from './profile/event/event.component';
import { AddComponent } from './profile/add/add.component';
import {QRCodeModule} from "angular2-qrcode";


@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    ConcertDetailComponent,
    LandingComponent,
    LoginFormComponent,
    RegisterComponent,
    InfoComponent,
    SideBarComponent,
    RegisterComponent,
    ProfileComponent,
    InfoComponent,
    BoughtComponent,
    EventComponent,
    AddComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    BrowserAnimationsModule,
    MDBBootstrapModule.forRoot(),
    FormsModule,
    MatCardModule,
    MatButtonModule,
    MatGridListModule,
    MatToolbarModule,
    FlexLayoutModule,
    HttpClientModule,
    MatListModule,
    ReactiveFormsModule,
    QRCodeModule
  ],
  providers: [UserService],
  bootstrap: [AppComponent],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class AppModule {
}
