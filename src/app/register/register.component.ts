import {Component} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../services/user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {

  regForm: FormGroup;

  constructor(private fb: FormBuilder, private userservice: UserService, private router: Router) {
    this.regForm = fb.group({
      defaultFormLogin: ['', [Validators.required]],
      defaultFormPass: ['', [Validators.required]],
      defaultFormName: ['', [Validators.required]],
      defaultFormSurname: ['', [Validators.required]],
      defaultFormEmail: ['', [Validators.required]]
    });
  }

  Reg() {
    if (this.regForm.invalid) {
      return;
    }

    const email = this.regForm.value.defaultFormEmail;
    const pass = this.regForm.value.defaultFormPass;
    const login = this.regForm.value.defaultFormLogin;
    const name = this.regForm.value.defaultFormName;
    const surname = this.regForm.value.defaultFormSurname;

    this.userservice.register(email, pass, login, name, surname).subscribe((err) => {
      if (err === 'error') {
        this.regForm.reset();
        return;
      }
      console.log(localStorage.getItem('token'));
      this.router.navigate(['/concerts']);
    });
  }

}
