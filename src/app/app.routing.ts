import {Routes} from '@angular/router';
import {MenuComponent} from "./menu/menu.component";
import {ConcertDetailComponent} from "./concert-detail/concert-detail.component";
import {LandingComponent} from "./landing/landing.component";
import {LoginFormComponent} from "./login-form/login-form.component";
import {RegisterComponent} from "./register/register.component";
import {ProfileComponent} from "./profile/profile.component";


export const appRoutes: Routes = [
  {
    path: '',
    component: LandingComponent
  },
  {
    path: 'login',
    component: LoginFormComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'profile',
    component: ProfileComponent
  },
  {
    path: 'concerts',
    children: [
      {
        path: ':id',
        children: [
          {
            path: 'buy',
            component: MenuComponent
          },
          {
            path: '',
            component: ConcertDetailComponent
          }
        ]

      },
      {
        path: '',
        component: MenuComponent
      }
    ]
  },
  {
    path: '**',
    redirectTo: '/'
  }
];
